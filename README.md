Examples of Dependent Haskell
=============================

This repo is a collection of buildable Haskell packages that serve as exemplars
of dependent types in Haskell. To keep the packages buildable, the `main` branch
will be written in Haskell code that compiles with the latest released GHC. Other
branches (names to be listed here when they are ready) will render the code
that should be allowed once we have various dependent-type features. Then, comparing
the branches can show a nice side-by-side comparison of how dependent types will
aid in Haskell programming.

Please feel free to submit PRs to expand the range of examples!
