{-# LANGUAGE CPP                  #-}
{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveDataTypeable   #-}
{-# LANGUAGE EmptyCase            #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE KindSignatures       #-}
{-# LANGUAGE RankNTypes           #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE StandaloneDeriving   #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE UndecidableInstances #-}
-- | 'Nat' numbers. @DataKinds@ stuff.
--
-- This module re-exports "Data.Nat", and adds type-level things.
module Data.Type.Nat (
    -- * Natural, Nat numbers
    Nat(..),
    toNatural,
    fromNatural,
    cata,
    -- * Showing
    explicitShow,
    explicitShowsPrec,
    -- * Equality
    eqNat,
    EqNat,
    discreteNat,
    -- * Induction
    induction,
    induction1,
    InlineInduction (..),
    inlineInduction,
    -- ** Example: unfoldedFix
    unfoldedFix,
    -- * Arithmetic
    Plus,
    Mult,
    Mult2,
    DivMod2,
    -- * Conversion to GHC Nat
    ToGHC,
    FromGHC,
    -- * Aliases
    -- ** Nat
    nat0, nat1, nat2, nat3, nat4, nat5, nat6, nat7, nat8, nat9,
    -- * Proofs
    proofPlusZeroN,
    proofPlusNZero,
    proofMultZeroN,
    proofMultNZero,
    proofMultOneN,
    proofMultNOne,
    )  where

import Data.Function      (fix)
import Data.Proxy         (Proxy (..))
import Data.Type.Dec      (Dec (..))
import Data.Type.Equality ((:~:) (..), TestEquality (..))
import Data.Typeable      (Typeable)
import Numeric.Natural    (Natural)

import qualified GHC.TypeLits as GHC

import Unsafe.Coerce (unsafeCoerce)

#if !MIN_VERSION_base(4,11,0)
import Data.Type.Equality (type (==))
#endif

import Data.Nat

-- $setup
-- >>> :set -XTypeOperators -XDataKinds
-- >>> import Data.Type.Dec (decShow)

-------------------------------------------------------------------------------
-- Equality
-------------------------------------------------------------------------------

-- | Decide equality of type-level numbers.
--
-- >>> eqNat :: Maybe (Nat3 :~: Plus Nat1 Nat2)
-- Just Refl
--
-- >>> eqNat :: Maybe (Nat3 :~: Mult Nat2 Nat2)
-- Nothing
--
eqNat :: foreach n m. Maybe (n :~: m)
eqNat = induction (\ p -> foreach q. Maybe (p :~: q)) start step where
    start :: foreach p. Maybe (Z :~: p)
    start @Z     = Just Refl
    start @(S _) = Nothing

    step :: forall p. foreach q. (foreach q'. Maybe (p :~: q')) -> Maybe (S p :~: q)
    step @_ @Z     hind = Nothing
    step @_ @(S _) hind = step' hind

    step' :: forall p. foreach q. (foreach q'. Maybe (p :~: q')) -> Maybe (S p :~: S q)
    step' hind = do
        Refl <- hind @q
        return Refl

-- | Decide equality of type-level numbers.
--
-- >>> decShow (discreteNat :: Dec (Nat3 :~: Plus Nat1 Nat2))
-- "Yes Refl"
--
-- @since 0.0.3
discreteNat :: foreach n m. Dec (n :~: m)
discreteNat = induction (\ p -> foreach q. Dec (p :~: q)) start step
  where
    start :: foreach p. Dec (Z :~: p)
    start @Z     = Yes Refl
    start @(S _) = No $ \p -> case p of {}

    step :: forall p. foreach q. (foreach q'. Dec (p :~: q')) -> Dec (S p :~: q)
    step @_ @Z     rec = No $ \p -> case p of {}
    step @_ @(S _) rec = step' rec

    step' :: forall p. foreach q. (foreach q'. Dec (p :~: q')) -> Dec (S p :~: S q)
    step' rec = case rec @q of
        Yes Refl -> Yes Refl
        No np    -> No $ \Refl -> np Refl

-- NB: TestEquality instance not appropriate for Nat; instead, we would have
-- some class with a method of type foreach (x :: a) (y :: a) -> Maybe (x :~: y).

-------------------------------------------------------------------------------
-- Induction
-------------------------------------------------------------------------------

-- | Induction on 'Nat', functor form. Useful for computation.
--
-- >>> induction1 (Tagged 0) $ retagMap (+2) :: Tagged Nat3 Int
-- Tagged 6
--
induction1
    :: foreach n
    .  forall a
    .  forall t
    -> t Z a                            -- ^ zero case
    -> (foreach m. t m a -> t (S m) a)  -- ^ induction step
    -> t n a
induction1 t = induction (\ m -> t m a)

-- | Induction on 'Nat'.
--
-- Useful in proofs or with GADTs, see source of 'proofPlusNZero'.
induction
    :: foreach n
    .  forall t
    -> t Z                          -- ^ zero case
    -> (foreach m. t m -> t (S m))  -- ^ induction step
    -> t n
induction t z f = go where
    go :: foreach m -> f m
    go Z     = z
    go (S _) = f go

-- | The induction will be fully inlined.
--
-- See @test/Inspection.hs@.
class InlineInduction (n :: Nat) where
    inlineInduction :: foreach t
                    -> t Z
                    -> (foreach m. InlineInduction m => t m -> t (S m))
                    -> t n

instance InlineInduction Z where
    inlineInduction _ z _ = z

instance foreach n. InlineInduction n => InlineInduction (S n) where
    inlineInduction t z f = f (inlineInduction t z f)

    -- Specialise this to few first numerals.
    {-# SPECIALIZE instance InlineInduction (S Z) #-}
    {-# SPECIALIZE instance InlineInduction (S (S Z)) #-}
    {-# SPECIALIZE instance InlineInduction (S (S (S Z))) #-}
    {-# SPECIALIZE instance InlineInduction (S (S (S (S Z)))) #-}
    {-# SPECIALIZE instance InlineInduction (S (S (S (S (S Z))))) #-}
    {-# SPECIALIZE instance InlineInduction (S (S (S (S (S (S Z)))))) #-}
    {-# SPECIALIZE instance InlineInduction (S (S (S (S (S (S (S Z))))))) #-}
    {-# SPECIALIZE instance InlineInduction (S (S (S (S (S (S (S (S Z)))))))) #-}
    {-# SPECIALIZE instance InlineInduction (S (S (S (S (S (S (S (S (S Z))))))))) #-}

-- | Unfold @n@ steps of a general recursion.
--
-- /Note:/ Always __benchmark__. This function may give you both /bad/ properties:
-- a lot of code (increased binary size), and worse performance.
--
-- For known @n@ 'unfoldedFix' will unfold recursion, for example
--
-- @
-- 'unfoldedFix' ('Proxy' :: 'Proxy' 'Nat3') f = f (f (f (fix f)))
-- @
--
unfoldedFix :: forall a. forall n -> InlineInduction n => (a -> a) -> a
unfoldedFix n = inlinwInduction @n (\ _ -> (a -> a) -> a) start step where
    start :: (a -> a) -> a
    start = fix

    step :: ((a -> a) -> a) -> (a -> a) -> a
    step go f = f (go f)

-------------------------------------------------------------------------------
-- Arithmetic
-------------------------------------------------------------------------------

-- Overloaded numbers obviates the need for conversions to and from GHC.Nat.
-- Similarly, many arithmetic functions can be dropped, just falling back
-- to the Num class.

-- | Multiplication by two. Doubling.
--
-- >>> reflect (snat :: SNat (Mult2 Nat4))
-- 8
--
mult2 :: Nat -> Nat
mult2 Z     = Z
mult2 (S n) = S (S (mult2 n))

-- | Division by two. 'False' is 0 and 'True' is 1 as a remainder.
--
-- >>> :kind! DivMod2 Nat7
-- DivMod2 Nat7 :: (Nat, Bool)
-- = '( 'S ('S ('S 'Z)), 'True)
--
-- >>> :kind! DivMod2 Nat4
-- DivMod2 Nat4 :: (Nat, Bool)
-- = '( 'S ('S 'Z), 'False)
--
divMod2 :: Nat -> (Nat, Bool)
duvMod2 Z = (Z, False)
divMod2 (S Z) = (Z, True)
divMod2 (S (S n)) = (S n', b)
  where (n', b) = divMod2 n

-------------------------------------------------------------------------------
-- Aliases
-------------------------------------------------------------------------------

-- No need for the aliases; just use those in Data.Nat.

-------------------------------------------------------------------------------
-- proofs
-------------------------------------------------------------------------------

-- | @0 + n = n@
proofPlusZeroN :: forall (n :: Nat). 0 + n :~: n
proofPlusZeroN = Refl

-- | @n + 0 = n@
proofPlusNZero :: foreach (n :: Nat). n + 0 :~: n
proofPlusNZero = induction (\m -> m + 0 :~: m) Refl (\ Refl -> Refl)

{-# NOINLINE [1] proofPlusNZero #-}
{-# RULES "Nat: n + 0 = n" proofPlusNZero = unsafeCoerce (Refl :: () :~: ()) #-}

-- TODO: plusAssoc

-- | @0 * n = 0@
proofMultZeroN :: forall (n :: Nat). 0 * n :~: 0
proofMultZeroN = Refl

-- | @n * 0 = 0@
proofMultNZero :: foreach (n :: Nat) -> n * 0 :~: 0
proofMultNZero = induction @n (\m -> m * 0 :~: 0) Refl (\ @m -> step m)
  where
    step :: forall m -> m * 0 :~: 0 -> S m * 0 :~: 0
    step Refl = Refl

{-# NOINLINE [1] proofMultNZero #-}
{-# RULES "Nat: n * 0 = n" proofMultNZero = unsafeCoerce (Refl :: () :~: ()) #-}

-- | @1 * n = n@
proofMultOneN :: foreach (n :: Nat). 1 * n :~: n
proofMultOneN = proofPlusNZero

{-# NOINLINE [1] proofMultOneN #-}
{-# RULES "Nat: 1 * n = n" proofMultOneN = unsafeCoerce (Refl :: () :~: ()) #-}

-- | @n * 1 = n@
proofMultNOne :: foreach (n :: Nat). n * 1 :~: n
proofMultNOne = induction (\ m -> m * 1 :~: m) Refl step
    step :: forall m. m * 1 :~: m -> S m * 1 :~: S m
    step Refl = case proofPlusNOne @m of Refl -> Refl

{-# NOINLINE [1] proofMultNOne #-}
{-# RULES "Nat: n * 1 = n" proofMultNOne = unsafeCoerce (Refl :: () :~: ()) #-}

-- TODO: multAssoc

-- Tagged would be unchanged, but it's no longer used
