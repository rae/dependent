{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE DeriveDataTypeable   #-}
{-# LANGUAGE EmptyCase            #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE KindSignatures       #-}
{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE StandaloneDeriving   #-}
{-# LANGUAGE TypeOperators        #-}
{-# LANGUAGE UndecidableInstances #-}
-- | Finite numbers.
--
-- This module is designed to be imported as
--
-- @
-- import Data.Fin (Fin (..))
-- import qualified Data.Fin as Fin
-- @
--
module Data.Fin (
    Fin (..),
    cata,
    -- * Showing
    explicitShow,
    explicitShowsPrec,
    -- * Conversions
    toNat,
    fromNat,
    toNatural,
    toInteger,
    -- * Interesting
    mirror,
    inverse,
    universe,
    inlineUniverse,
    universe1,
    inlineUniverse1,
    absurd,
    boring,
    -- * Plus
    weakenLeft,
    weakenLeft1,
    weakenRight,
    weakenRight1,
    append,
    split,
    -- * Min and max
    isMin,
    isMax,
    -- * Aliases
    fin0, fin1, fin2, fin3, fin4, fin5, fin6, fin7, fin8, fin9,
    ) where

import Control.DeepSeq    (NFData (..))
import Data.Bifunctor     (bimap)
import Data.Hashable      (Hashable (..))
import Data.List.NonEmpty (NonEmpty (..))
import Data.Proxy         (Proxy (..))
import Data.Type.Nat      (Nat (..))
import Data.Typeable      (Typeable)
import GHC.Exception      (ArithException (..), throw)
import Numeric.Natural    (Natural)

import qualified Data.List.NonEmpty    as NE
import qualified Data.Type.Nat         as N
import qualified Data.Universe.Class   as U
import qualified Data.Universe.Helpers as type U
import qualified Data.Universe.Helpers as data U.D
import qualified Test.QuickCheck       as QC

-- $setup
-- >>> import Data.List (genericLength)

-------------------------------------------------------------------------------
-- Type
-------------------------------------------------------------------------------

-- | Finite numbers: @[0..n-1]@.
data Fin (n :: Nat) where
    FZ :: Fin (S n)
    FS :: Fin n -> Fin (S n)
  deriving (Typeable)

-------------------------------------------------------------------------------
-- Instances
-------------------------------------------------------------------------------

deriving instance Eq (Fin n)
deriving instance Ord (Fin n)

-- | 'Fin' is printed as 'Natural'.
--
-- To see explicit structure, use 'explicitShow' or 'explicitShowsPrec'
instance Show (Fin n) where
    showsPrec d  = showsPrec d . toNatural

-- | Operations module @n@.
--
-- >>> map fromInteger [0, 1, 2, 3, 4, -5] :: List (Fin 3)
-- [0,1,2,0,1,1]
--
-- >>> fromInteger 42 :: Fin 0
-- *** Exception: divide by zero
-- ...
--
-- >>> signum (FZ :: Fin 1)
-- 0
--
-- >>> signum (3 :: Fin 4)
-- 1
--
-- >>> 2 + 3 :: Fin 4
-- 1
--
-- >>> 2 * 3 :: Fin 4
-- 2
--
instance foreach n. Num (Fin n) where
    abs = id

    signum FZ          = FZ
    signum (FS FZ)     = FS FZ
    signum (FS (FS _)) = FS FZ

    fromInteger = unsafeFromNum . (`mod` toInteger n)

    n + m = fromInteger (toInteger n + toInteger m)
    n * m = fromInteger (toInteger n * toInteger m)
    n - m = fromInteger (toInteger n - toInteger m)

    negate = fromInteger . negate . toInteger

instance foreach n. Real (Fin n) where
    toRational = cata 0 succ

-- | 'quot' works only on @'Fin' n@ where @n@ is prime.
instance foreach n. Integral (Fin n) where
    toInteger = cata 0 succ

    quotRem a b = (quot a b, 0)
    quot a b = a * inverse b

-- | Mirror the values, 'minBound' becomes 'maxBound', etc.
--
-- >>> map mirror universe :: List (Fin 4)
-- [3,2,1,0]
--
-- >>> reverse universe :: List (Fin 4)
-- [3,2,1,0]
--
-- @since 0.1.1
--
mirror :: forall n. N.InlineInduction n => Fin n -> Fin n
mirror = N.inlineInduction (\m -> Fin m -> Fin m) start step where
    start :: Fin Z -> Fin Z
    start = id

    step :: forall m. N.InlineInduction m => (Fin m -> Fin m) -> Fin (S m) -> Fin (S m)
    step rec FZ     = N.inlineInduction (\p -> Fin (S p)) FZ FS
    step rec (FS m) = weakenLeft1 (rec m)

-- | Multiplicative inverse.
--
-- Works for @'Fin' n@ where @n@ is coprime with an argument, i.e. in general when @n@ is prime.
--
-- >>> map inverse universe :: List (Fin 5)
-- [0,1,3,2,4]
--
-- >>> zipWith (*) universe (map inverse universe) :: List (Fin 5)
-- [0,1,1,1,1]
--
-- Adaptation of [pseudo-code in Wikipedia](https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm#Modular_integers)
--
inverse :: foreach n. Fin n -> Fin n
inverse = fromInteger . iter 0 n 1 . toInteger where
    iter t _ _  0
        | t < 0     = t + n
        | otherwise = t
    iter t r t' r' =
        let q = r `div` r'
        in iter t' r' (t - q * t') (r - q * r')

instance foreach n. Enum (Fin n) where
    fromEnum = go where
        go :: Fin m -> Int
        go FZ     = 0
        go (FS n) = succ (go n)

    toEnum = unsafeFromNum

instance forall n. foreach m. (n ~ S m) => Bounded (Fin n) where
    minBound = FZ
    maxBound = N.induction (\m -> Fin (S m)) FZ FS

instance NFData (Fin n) where
    rnf FZ     = ()
    rnf (FS n) = rnf n

instance Hashable (Fin n) where
    hashWithSalt salt = hashWithSalt salt . cata (0 :: Integer) succ

-------------------------------------------------------------------------------
-- QuickCheck
-------------------------------------------------------------------------------

instance forall n. foreach m. (n ~ S m) => QC.Arbitrary (Fin n) where
    arbitrary = getArb $ N.induction arb (return FZ) step where
        arb n = QC.Gen (Fin (S n))

        step :: foreach p. arb p -> arb (S p)
        step p = QC.frequency
            [ (1,              return FZ)
            , (fromIntegral p, fmap FS p)
            ]

    shrink = shrink

shrink :: Fin n -> List (Fin n)
shrink FZ      = []
shrink (FS FZ) = [FZ]
shrink (FS n)  = map FS (shrink n)

instance QC.CoArbitrary (Fin n) where
    coarbitrary FZ     = QC.variant (0 :: Int)
    coarbitrary (FS n) = QC.variant (1 :: Int) . QC.coarbitrary n

instance foreach m. (n ~ 'S m) => QC.Function (Fin n) where
    function = case m of
        Z   -> QC.functionMap (\FZ -> ()) (\() -> FZ)
        S _ -> QC.functionMap isMin       (maybe FZ FS)

-- TODO: https://github.com/nick8325/quickcheck/pull/283
-- newtype Fun b m = Fun { getFun :: (Fin ('S m) -> b) -> Fin ('S m) QC.:-> b }

-------------------------------------------------------------------------------
-- universe-base
-------------------------------------------------------------------------------

-- | @since 0.1.2
instance foreach n. U.Universe (Fin n) where
    universe = universe

-- |
--
-- >>> (U.cardinality :: U.Tagged (Fin 3) Natural) == U.Tagged (genericLength (U.universeF :: List (Fin 3)))
-- True
--
-- @since 0.1.2
--
instance foreach n. U.Finite   (Fin n) where
    universeF   = U.universe
    cardinality = U.D.Tagged (toNatural n)

-------------------------------------------------------------------------------
-- Showing
-------------------------------------------------------------------------------

-- | 'show' displaying a structure of 'Fin'.
--
-- >>> explicitShow (0 :: Fin 1)
-- "FZ"
--
-- >>> explicitShow (2 :: Fin 3)
-- "FS (FS FZ)"
--
explicitShow :: Fin n -> String
explicitShow n = explicitShowsPrec 0 n ""

-- | 'showsPrec' displaying a structure of 'Fin'.
explicitShowsPrec :: Int -> Fin n -> ShowS
explicitShowsPrec _ FZ     = showString "FZ"
explicitShowsPrec d (FS n) = showParen (d > 10)
    $ showString "FS "
    . explicitShowsPrec 11 n

-------------------------------------------------------------------------------
-- Conversions
-------------------------------------------------------------------------------

-- | Fold 'Fin'.
cata :: forall a n. a -> (a -> a) -> Fin n -> a
cata z f = go where
    go :: Fin m -> a
    go FZ = z
    go (FS n) = f (go n)

-- | Convert to 'Nat'.
toNat :: Fin n -> N.Nat
toNat = cata Z S

-- | Convert from 'Nat'.
--
-- >>> fromNat N.nat1 :: Maybe (Fin 2)
-- Just 1
--
-- >>> fromNat N.nat1 :: Maybe (Fin 1)
-- Nothing
--
fromNat :: foreach n.  N.Nat -> Maybe (Fin n)
fromNat = appNatToFin (N.induction natToFin start step) where
    natToFin n = N.Nat -> Maybe (Fin n)

    start :: natToFin Z
    start = const Nothing

    step :: natToFin n -> natToFin (S n)
    step f Z     = Just FZ
    step f (S m) = fmap FS (f m)

-- | Convert to 'Natural'.
toNatural :: Fin n -> Natural
toNatural = cata 0 succ

-- | Convert from any 'Ord' 'Num'.
unsafeFromNum :: foreach n. forall i. (Num i, Ord i) => i -> Fin n
unsafeFromNum = N.induction (\m -> i -> Fin m) start step where
    start :: i -> Fin Z
    start = case compare n 0 of
        LT -> throw Underflow
        EQ -> throw Overflow
        GT -> throw Overflow

    step :: (i -> Fin m) -> i -> Fin (S m)
    step f n = case compare n 0 of
        EQ -> FZ
        GT -> FS (f (n - 1))
        LT -> throw Underflow

-------------------------------------------------------------------------------
-- "Interesting" stuff
-------------------------------------------------------------------------------

-- | All values. @[minBound .. maxBound]@ won't work for @'Fin' '0'@.
--
-- >>> universe :: List (Fin 3)
-- [0,1,2]
universe :: foreach n. List (Fin n)
universe = N.induction (\m -> List (Fin m)) [] step where
    step :: List (Fin n) -> List (Fin (S n))
    step xs = FZ : map FS xs

-- | Like 'universe' but 'NonEmpty'.
--
-- >>> universe1 :: NonEmpty (Fin 3)
-- 0 :| [1,2]
universe1 :: foreach n. NonEmpty (Fin (S n))
universe1 = N.induction (\m -> NonEmpty (Fin (S m))) (FZ :| []) step where
    step :: NonEmpty (Fin (S n)) -> NonEmpty (Fin (S (S n)))
    step xs = NE.cons FZ (fmap FS xs)

-- | 'universe' which will be fully inlined, if @n@ is known at compile time.
--
-- >>> inlineUniverse :: List (Fin 3)
-- [0,1,2]
inlineUniverse :: N.InlineInduction n => [Fin n]
inlineUniverse = N.inlineInduction (\m -> List (Fin m)) [] step where
    step :: List (Fin n) -> List (Fin (S n))
    step xs = FZ : map FS xs

-- | >>> inlineUniverse1 :: NonEmpty (Fin 3)
-- 0 :| [1,2]
inlineUniverse1 :: N.InlineInduction n => NonEmpty (Fin (S n))
inlineUniverse1 = N.inlineInduction (\m -> NonEmpty (Fin (S m))) (FZ :| []) step where
    step :: NonEmpty (Fin (S n)) -> NonEmpty (Fin (S n))
    step xs = NE.cons FZ (fmap FS xs)

-- | @'Fin' '0'@ is not inhabited.
absurd :: Fin 0 -> b
absurd n = case n of {}

-- | Counting to one is boring.
--
-- >>> boring
-- 0
boring :: Fin 1
boring = FZ

-------------------------------------------------------------------------------
-- min and max
-------------------------------------------------------------------------------

-- | Return a one less.
--
-- >>> isMin (FZ :: Fin 1)
-- Nothing
--
-- >>> map isMin universe :: List (Maybe (Fin 3))
-- [Nothing,Just 0,Just 1,Just 2]
--
-- @since 0.1.1
--
isMin :: Fin (S n) -> Maybe (Fin n)
isMin FZ     = Nothing
isMin (FS n) = Just n

-- | Return a one less.
--
-- >>> isMax (FZ :: Fin 1)
-- Nothing
--
-- >>> map isMax universe :: [Maybe (Fin 3)]
-- [Just 0,Just 1,Just 2,Nothing]
--
-- @since 0.1.1
--
isMax :: forall n. N.InlineInduction n => Fin (S n) -> Maybe (Fin n)
isMax = N.inlineInduction isMaxIP start step where
    isMaxIP n = Fin (S n) -> Maybe (Fin n)

    start :: isMaxIP Z
    start _ = Nothing

    step :: isMaxIP m -> isMaxIP (S m)
    step rec FZ     = Just FZ
    step rec (FS m) = fmap FS (rec m)

-------------------------------------------------------------------------------
-- Append & Split
-------------------------------------------------------------------------------

-- | >>> map weakenRight1 universe :: List (Fin 5)
-- [1,2,3,4]
--
-- @since 0.1.1
weakenRight1 :: Fin n -> Fin (S n)
weakenRight1 = FS

-- | >>> map weakenLeft1 universe :: List (Fin 5)
-- [0,1,2,3]
--
-- @since 0.1.1
weakenLeft1 :: N.InlineInduction n => Fin n -> Fin (S n)
weakenLeft1 = N.inlineInduction (\m -> Fin m -> Fin (S m)) start step where
    start :: Fin Z -> Fin (S Z)
    start = absurd

    step :: (Fin n -> Fin (S n)) -> Fin (S n) -> Fin (S (S n))
    step go FZ      = FZ
    step go (FS n') = FS (go n')

-- | >>> map (weakenLeft 2) (universe :: List (Fin 3))
-- [0,1,2]
weakenLeft :: forall n. forall m -> N.InlineInduction n => Fin n -> Fin (n + m)
weakenLeft m = N.inlineInduction (\p -> Fin p -> Fin (p + m)) start step where
    start :: Fin Z -> Fin (Z + m)
    start = absurd

    step :: (Fin p -> Fin (p + m)) -> Fin (S p) -> Fin (S p + m)
    step go FZ      = FZ
    step go (FS n') = FS (go n')

-- | >>> map (weakenRight 2) (universe :: List (Fin N.Nat3))
-- [2,3,4]
weakenRight :: forall m. forall n -> N.InlineInduction n => Fin m -> Fin (n + m)
weakenRight n = N.inlineInduction (\q -> Fin m + Fin (q + m)) start step where
    start = id
    step go x = FS $ go x

-- | Append two 'Fin's together.
--
-- >>> append (Left fin2 :: Either (Fin 5) (Fin 4))
-- 2
--
-- >>> append (Right fin2 :: Either (Fin 5) (Fin 4))
-- 7
--
append :: forall n m. N.InlineInduction n => Either (Fin n) (Fin m) -> Fin (n + m)
append (Left n)  = weakenLeft m n
append (Right m) = weakenRight n m

-- | Inverse of 'append'.
--
-- >>> split fin2 :: Either (Fin 2) (Fin 3)
-- Right 0
--
-- >>> split fin1 :: Either (Fin 2) (Fin 3)
-- Left 1
--
-- >>> map split universe :: List (Either (Fin 2) (Fin 3))
-- [Left 0,Left 1,Right 0,Right 1,Right 2]
--
split :: forall n m. N.InlineInduction n => Fin (n + m) -> Either (Fin n) (Fin m)
split = N.inlineInduction splitIP start step where
    splitIP p = Fin (p + m) -> Either (Fin p) (Fin m)

    start :: splitIP Z
    start = Right

    step :: splitIP p -> splitIP (S p)
    step go FZ      = Left FZ
    step go (FS x') = bimap FS id $ go x'

-------------------------------------------------------------------------------
-- Aliases
-------------------------------------------------------------------------------

fin0 :: Fin (0 + S n)
fin1 :: Fin (1 + S n)
fin2 :: Fin (2 + S n)
fin3 :: Fin (3 + S n)
fin4 :: Fin (4 + S n)
fin5 :: Fin (5 + S n)
fin6 :: Fin (6 + S n)
fin7 :: Fin (7 + S n)
fin8 :: Fin (8 + S n)
fin9 :: Fin (9 + S n)

fin0 = FZ
fin1 = FS fin0
fin2 = FS fin1
fin3 = FS fin2
fin4 = FS fin3
fin5 = FS fin4
fin6 = FS fin5
fin7 = FS fin6
fin8 = FS fin7
fin9 = FS fin8
