module DH.Data.Type.Dec where

open import DH.Prelude

-- | Intuitionistic negation.
Neg : Set ℓ → Set ℓ
Neg a = a → ⊥

-- | Decidable (nullary) relations.
data Dec (a : Set ℓ) : Set ℓ where
  Yes : a → Dec a
  No : Neg a → Dec a

-- | Class of decidable types.
--
-- == Law
--
-- @a@ should be a Proposition, i.e. the 'Yes' answers should be unique.
--
-- /Note:/ We'd want to have decidable equality @:~:@ here too,
-- but that seems to be a deep dive into singletons.
record Decidable (a : Set ℓ) : Set ℓ where
  field
    decide : Dec a

open Decidable {{...}} public

-------------------------------------------------------------------------------
-- Neg combinators
-------------------------------------------------------------------------------

-- | We can negate anything twice.
--
-- Double-negation elimination is inverse of 'toNegNeg' and generally
-- impossible.
toNegNeg : a → Neg (Neg a)
toNegNeg x = (λ f → f $ x)

-- | Triple negation can be reduced to a single one.
tripleNeg : Neg (Neg (Neg a)) → Neg a
tripleNeg f a = f (toNegNeg a)

-- | Weak contradiction.
contradict : (a → Neg b) → b → Neg a
contradict f b a = f a b

-- | A variant of contraposition.
contraposition : (a → b) → Neg b → Neg a
contraposition f nb a = nb (f a)

-------------------------------------------------------------------------------
-- Dec combinators
-------------------------------------------------------------------------------

eqDec : {{_ : Eq a}} → Dec a → Dec a → Bool
eqDec (Yes x) (Yes x₁) = x == x₁
eqDec (Yes x) (No x₁) = false
eqDec (No x) (Yes x₁) = false
eqDec (No x) (No x₁) = true  -- @'Not a' is a /h-Prop/ so all values are equal.

instance
  EqDec : {{_ : Eq a}} → Eq (Dec a)
  EqDec = record { _==_ = eqDec }

cmpDec : {{_ : Ord a}} → Dec a → Dec a → Ordering
cmpDec (Yes x) (Yes x₁) = compare x x₁
cmpDec (Yes x) (No x₁) = compare false true
cmpDec (No x) (Yes x₁) = compare true false
cmpDec (No x) (No x₁) = EQ


-- | 'decToBool' respects this ordering.
--
-- /Note:/ yet if you have @p :: a@ and @p :: 'Neg' a@, something is wrong.
--
instance
  OrdDec : {{_ : Ord a}} → Ord (Dec a)
  OrdDec = record { compare = cmpDec }

-- | Flip 'Dec' branches.
decNeg : Dec a → Dec (Neg a)
decNeg (Yes p) = No (toNegNeg p)
decNeg (No np) = Yes np

-- | Convert @'Dec' a@ to @'Maybe' a@, forgetting the 'No' evidence.
decToMaybe : Dec a → Maybe a
decToMaybe (Yes p) = Just p
decToMaybe (No _)  = Nothing

-- | Convert 'Dec' to 'Bool', forgetting the evidence.
decToBool : Dec a → Bool
decToBool (Yes _) = true
decToBool (No _)  = false

-------------------------------------------------------------------------------
-- Decidable equality
-------------------------------------------------------------------------------

-- This is meant as a replacement for TestEquality; the {{_ : Eq a}} is like
-- a superclass constraint
record EqD (a : Set ℓ) {{_ : Eq a}} : Set ℓ where
  field
    _===_ : (p : a) → (q : a) → Dec (p ≡ q)
    
open EqD {{...}} public

eqDℕ : ∀ (p q : ℕ) → Dec (p ≡ q)
eqDℕ ℕ.zero ℕ.zero = Yes refl
eqDℕ ℕ.zero (ℕ.suc n2) = No $ λ ()
eqDℕ (ℕ.suc n1) ℕ.zero = No $ λ ()
eqDℕ (ℕ.suc n1) (ℕ.suc n2) with eqDℕ n1 n2
... | Yes refl = Yes refl
... | No contra = No λ scontra → contra (unsuc scontra) where
  unsuc : ∀ {p q : ℕ} → ℕ.suc p ≡ ℕ.suc q → p ≡ q
  unsuc refl = refl

instance
  EqDℕ : EqD ℕ
  EqDℕ = record { _===_ = eqDℕ }
