-- |
--
-- This module is designed to be imported qualified:
--
-- @
-- import qualified Data.Fin.Enum as E
-- @
--
module DH.Data.Fin.Enum where

open import DH.Prelude hiding (Enum; EnumBool)
open import DH.Data.Nat
open import DH.Data.Type.Nat using ( InlineInduction )
open import DH.Data.Fin

import DH.Generics as G
open G hiding ( from; to )

-------------------------------------------------------------------------------
-- EnumSize
-------------------------------------------------------------------------------

-- | Compute the size from the type.
EnumSizeRep : RepType → Nat → Nat
EnumSizeRep (a :+: b) n = EnumSizeRep a (EnumSizeRep b n)
EnumSizeRep V1        n = n
EnumSizeRep (M1 _c a) n = EnumSizeRep a n
EnumSizeRep U1        n = S n
-- No instance for K1 or :*:
EnumSizeRep (a :*: b) n = throw StuckTypeFamily

GEnumSize : ∀ (a : Set ℓ) → {{_ : Generic a}} → Nat
GEnumSize a = EnumSizeRep (Rep a) 0

-------------------------------------------------------------------------------
-- From
-------------------------------------------------------------------------------

record GFromRep (a : RepType) : Set where
  field
    gfromRep   : I a → ∀ n -> Fin (EnumSizeRep a n)
    gfromSkipI : Fin n -> Fin (EnumSizeRep a n)

open GFromRep {{...}} public

gfromSkip : ∀ (a : RepType) {n : Nat} → {{_ : GFromRep a}} → Fin n → Fin (EnumSizeRep a n)
gfromSkip a n = gfromSkipI {a = a} n

-- | Constraint for the class that computes 'gfrom'.
GFrom : (a : Set ℓ) → {{_ : Generic a}} → Set
GFrom a = GFromRep (Rep a)

-- | Generic version of 'from'.
gfrom : {{_ : Generic a}} → {{_ : GFrom a}} → a -> Fin (GEnumSize a)
gfrom x = gfromRep (G.from x) 0

gfromRep:+: : ∀ {a b : RepType} → {{_ : GFromRep a}} → {{_ : GFromRep b}}
            → I (a :+: b) → ∀ n → Fin (EnumSizeRep (a :+: b) n)
gfromRep:+: {b = b} (Left a) n = gfromRep a (EnumSizeRep b n)
gfromRep:+: {a = a} (Right b) n = gfromSkip a (gfromRep b n)

gfromSkip:+: : ∀ {a b : RepType} → {{_ : GFromRep a}} → {{_ : GFromRep b}}
             → Fin n → Fin (EnumSizeRep (a :+: b) n)
gfromSkip:+: {a = a} {b = b} n = gfromSkip a (gfromSkip b n)

instance
  GFromRep:+: : ∀ {a b : RepType} → {{_ : GFromRep a}} → {{_ : GFromRep b}}
              → GFromRep (a :+: b)
  GFromRep:+: {a = a} {b = b} = record { gfromRep = gfromRep:+:
                                       ; gfromSkipI = gfromSkip:+: {a = a} {b = b} }

instance
  GFromRepM1 : ∀ {a : RepType} → {m : Meta} → {{_ : GFromRep a}} → GFromRep (M1 m a)
  GFromRepM1 {a = a} = record { gfromRep = gfromRep {a = a}
                              ; gfromSkipI = λ n → gfromSkip a n }

instance
  GFromRepV1 : GFromRep V1
  GFromRepV1 = record { gfromRep = λ ()
                      ; gfromSkipI = λ n → n }

instance
  GFromRepU1 : GFromRep U1
  GFromRepU1 = record { gfromRep = λ _ _ → FZ
                      ; gfromSkipI = λ n → FS n }

-------------------------------------------------------------------------------
-- To
-------------------------------------------------------------------------------

record GToRep (a : RepType) : Set₁ where
  field
    gtoRep : ∀ {n} {r : Set} → Fin (EnumSizeRep a n) → (I a → r) → (Fin n → r) → r

open GToRep {{...}} public

-- | Constraint for the class that computes 'gto'.
GTo : (a : Set ℓ) → {{_ : Generic a}} → Set₁
GTo a = GToRep (G.Rep a)

-- | Generic version of 'to'.
gto : {{_ : Generic a}} → {{_ : GTo a}} → Fin (GEnumSize a) → a
gto x = G.to $ gtoRep x id absurd

instance
  GToRep:+: : ∀ {a b : RepType} → {{_ : GToRep a}} → {{_ : GToRep b}}
            → GToRep (a :+: b)
  GToRep:+: = record { gtoRep = λ n s k → gtoRep n (s ∘ Left) $ λ r → gtoRep r (s ∘ Right) k }

instance
  GToRepM1 : ∀ {a : RepType} {m : Meta} → {{_ : GToRep a}} → GToRep (M1 m a)
  GToRepM1 {a = a} = record { gtoRep = λ n s → gtoRep {a = a} n s }

instance
  GToRepV1 : GToRep V1
  GToRepV1 = record { gtoRep = λ n _ k → k n }

gtoRepU1 : ∀ {n} {r : Set} → Fin (EnumSizeRep U1 n) → (⊤ → r) → (Fin n → r) → r
gtoRepU1 FZ    s _ = s tt
gtoRepU1 (FS n) _ k = k n

instance
  GToRepU1 : GToRep U1
  GToRepU1 = record { gtoRep = gtoRepU1 }

-------------------------------------------------------------------------------
-- Enum
-------------------------------------------------------------------------------

-- | Generic enumerations.
--
-- /Examples:/
--
-- >>> from ()
-- 0
--
-- >>> to 0 :: ()
-- ()
--
-- >>> to 0 :: Bool
-- False
--
-- >>> map to F.universe :: [Bool]
-- [False,True]
--
-- >>> map (to . (+1) . from) [LT, EQ, GT] :: [Ordering] -- Num Fin is modulo arithmetic
-- [EQ,GT,LT]
--
record Enum (a : Set ℓ) : Set ℓ where
  field
    -- | The size of an enumeration.
    EnumSizeI : Nat

    -- | Converts a value to its index.
    from : a → Fin EnumSizeI

    -- | Converts from index to the original value.
    to : Fin EnumSizeI → a

open Enum {{...}} public

EnumSize : ∀ (a : Set ℓ) → {{_ : Enum a}} → Nat
EnumSize a = EnumSizeI {a = a}

defaultEnum : ∀ {a : Set ℓ} → {{_ : Generic a}} → {{_ : GFrom a}} → {{_ : GTo a}}
            → Enum a
defaultEnum {a = a} = record { EnumSizeI = GEnumSize a
                             ; from = gfrom
                             ; to = gto }

-- | 'Void' ~ 0
instance
  Enum⊥ : Enum ⊥
  Enum⊥ = record { EnumSizeI = 0
                 ; from = λ ()
                 ; to = λ () }
  
-- | () ~ 1
instance
  Enum⊤ : Enum ⊤
  Enum⊤ = defaultEnum

-- | 'Bool' ~ 2
instance
  EnumBool : Enum Bool
  EnumBool = defaultEnum

-- | 'Ordering' ~ 3
instance
  EnumOrdering : Enum Ordering
  EnumOrdering = defaultEnum

-- | 'Either' ~ @+@
instance
  EnumEither : ∀ {a b : Set} → {{EnumA : Enum a}} → {{EnumB : Enum b}}
             → {{_ : InlineInduction (EnumSize a)}}
             → Enum (Either a b)
  EnumEither {a = a} {b = b} {{EnumA = EnumA}} {{EnumB = EnumB}}
    = record { EnumSizeI = EnumSize a + EnumSize b
             ; to = bimap to to ∘ split
             ; from = append ∘ bimap from from }


