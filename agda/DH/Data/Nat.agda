module DH.Data.Nat where

open import DH.Prelude

-------------------------------------------------------------------------------
-- Nat
-------------------------------------------------------------------------------

-- | Nat natural numbers.
--
-- Better than GHC's built-in 'GHC.TypeLits.Nat' for some use cases.
--
data Nat : Set where
  Z : Nat
  S : Nat → Nat

variable
  n : Nat

eqNat : Nat → Nat → Bool
eqNat Z Z = true
eqNat Z (S n2) = false
eqNat (S n1) Z = false
eqNat (S n1) (S n2) = eqNat n1 n2

cmpNat : Nat → Nat → Ordering
cmpNat Z Z = EQ
cmpNat Z (S n2) = LT
cmpNat (S n1) Z = GT
cmpNat (S n1) (S n2) = cmpNat n1 n2

plusNat : Nat → Nat → Nat
plusNat Z n2 = n2
plusNat (S n1) n2 = S (plusNat n1 n2)

minusNat : Nat → Nat → Nat
minusNat n1 Z = n1
minusNat Z (S n2) = Z
minusNat (S n1) (S n2) = minusNat n1 n2

timesNat : Nat → Nat → Nat
timesNat Z n2 = Z
timesNat (S n1) n2 = plusNat (timesNat n1 n2) n2

negateNat : Nat → Nat
negateNat n1 = Z

absNat : Nat → Nat
absNat n1 = n1

signumNat : Nat → Nat
signumNat Z = Z
signumNat (S n1) = S Z

fromℕNat : ℕ → Nat
fromℕNat ℕ.zero = Z
fromℕNat (ℕ.suc n) = S (fromℕNat n)

instance
  EqNat : Eq Nat
  EqNat = record { _==_ = eqNat }

instance
  OrdNat : Ord Nat
  OrdNat = record { compare = cmpNat }

instance
  NumNat : Num Nat
  NumNat = record { _+_ = plusNat
                  ; _-_ = minusNat
                  ; _*_ = timesNat
                  ; negate = negateNat
                  ; abs = absNat
                  ; signum = signumNat
                  ; fromℕ = fromℕNat }

toEnumNat : ℤ → Nat
toEnumNat = fromℕNat ∘ fromℤ

NatToℕ : Nat → ℕ
NatToℕ Z = ℕ.zero
NatToℕ (S n) = ℕ.suc (NatToℕ n)

fromEnumNat : Nat → ℤ
fromEnumNat = toℤ ∘ NatToℕ

instance
  EnumNat : Enum Nat
  EnumNat = record { toEnum = toEnumNat; fromEnum = fromEnumNat }

quotRemNat : Nat → Nat → Nat × Nat
quotRemNat n1 n2 with quotRem (NatToℕ n1) (NatToℕ n2)
... | 𝕟1 , 𝕟2 = fromℕ 𝕟1 , fromℕ 𝕟2

instance
  IntegralNat : Integral Nat
  IntegralNat = record { toℤ = λ n → toℤ (fromEnumNat n)
                       ; fromℤ = λ z → toEnumNat (fromℤ z)
                       ; quotRem = quotRemNat }

-------------------------------------------------------------------------------
-- Conversions
-------------------------------------------------------------------------------

-- | Fold 'Nat'.
--
-- >>> cata [] ('x' :) 2
-- "xx"
--
cata : ∀ {a : Set ℓ} → a -> (a -> a) -> Nat -> a
cata {a = a} z f = go where
    go : Nat → a
    go Z     = z
    go (S n) = f (go n)
    
