-- cribbed from GHC.Generics

module DH.Generics where

open import DH.Prelude

record Meta : Set where

data RepType : Set where
  _:+:_ : RepType → RepType → RepType
  _:*:_ : RepType → RepType → RepType
  V1    : RepType
  M1    : Meta → RepType → RepType
  U1    : RepType
infixr 5 _:+:_ _:*:_

I : RepType → Set
I (r :+: r₁) = Either (I r) (I r₁)
I (r :*: r₁) = I r × I r₁
I V1 = ⊥
I (M1 x r) = I r
I U1 = ⊤

record Generic (a : Set ℓ) : Set ℓ where
  field
    RepI : RepType
    from : a → I RepI
    to : I RepI → a

open Generic {{...}} public

Rep : ∀ (a : Set ℓ) → {{_ : Generic a}} → RepType
Rep a = RepI {a = a}

instance
  Generic⊤ : Generic ⊤
  Generic⊤ = record { RepI = U1
                    ; from = id
                    ; to = id }

fromBool : Bool → Either ⊤ ⊤
fromBool false = Left tt
fromBool true = Right tt

toBool : Either ⊤ ⊤ → Bool
toBool (Left tt) = false
toBool (Right tt) = true

instance
  GenericBool : Generic Bool
  GenericBool = record { RepI = U1 :+: U1
                       ; from = fromBool
                       ; to = toBool }

fromOrdering : Ordering → Either ⊤ (Either ⊤ ⊤) 
fromOrdering LT = Left tt
fromOrdering EQ = Right (Left tt)
fromOrdering GT = Right (Right tt)

toOrdering : Either ⊤ (Either ⊤ ⊤) → Ordering
toOrdering (Left tt) = LT
toOrdering (Right (Left tt)) = EQ
toOrdering (Right (Right tt)) = GT

instance
  GenericOrdering : Generic Ordering
  GenericOrdering = record { RepI = U1 :+: U1 :+: U1
                           ; from = fromOrdering
                           ; to = toOrdering } 

